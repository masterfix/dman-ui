FROM node:lts-alpine as builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build-prod


FROM nginx:stable-alpine

COPY --from=builder /app/dist/dman-ui/ /usr/share/nginx/html
COPY misc/docker/nginx.conf /etc/nginx/conf.d/default.conf